function highlightElement(element){
    //remove the class for all elements
    var elems = document.querySelectorAll(".meditation-length.hightlight");

    [].forEach.call(elems, function(el) {
        console.log(el);
        el.classList.remove("hightlight");
    });
    //add class to selected element
    element.classList.add("hightlight");
}

window.onload = function () {
    list = document.getElementsByClassName("meditation-length");
    for (var i = 0; i < list.length; i++) {
        list[i].addEventListener("click", function (e) {
            e.preventDefault();
            var meditationLength = this.getAttribute('data-value');
            localStorage.setItem('meditationLength', meditationLength);
            highlightElement(this);
        });
    }
};